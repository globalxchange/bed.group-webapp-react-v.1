import React, { useState } from 'react';
import AboutFrag from './AboutFrag';
import Player from './Player';

const ViralTV = ({ colorCode, selectedApp, onClose }) => {
  const [activeTab, setActiveTab] = useState(TABS[0]);

  return (
    <div className="viral-tv-wrapper" style={{ color: colorCode }}>
      <div className="header-container">
        <div className="back-btn">
          <svg
            width="448"
            height="385"
            viewBox="0 0 448 385"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
            onClick={onClose}
          >
            <path
              d="M420.361 161.229C418.531 160.932 416.679 160.795 414.826 160.819H99.305L106.185 157.619C112.91 154.436 119.028 150.104 124.265 144.819L212.745 56.339C224.398 45.215 226.356 27.32 217.385 13.939C206.944 -0.320007 186.921 -3.416 172.661 7.025C171.509 7.869 170.414 8.78899 169.385 9.77899L9.38501 169.779C-3.11899 182.269 -3.12999 202.53 9.36001 215.034C9.36801 215.042 9.37701 215.051 9.38501 215.059L169.385 375.059C181.899 387.538 202.16 387.51 214.64 374.996C215.622 374.011 216.539 372.963 217.385 371.859C226.356 358.478 224.398 340.583 212.745 329.459L124.425 240.819C119.73 236.119 114.332 232.178 108.425 229.139L98.825 224.819H413.065C429.412 225.426 443.754 214.007 446.825 197.939C449.654 180.494 437.806 164.059 420.361 161.229Z"
              fill={colorCode || 'black'}
            />
          </svg>
        </div>
        <img
          src={selectedApp?.other_data?.coloredfulllogo}
          alt=""
          className="header-logo"
        />
        {/* <div className="header-action ml-auto">
          <img
            src={require('../../assets/redirect-icon.svg').default}
            alt=""
            className="header-action-icon"
          />
        </div> */}
      </div>
      <Player colorCode={colorCode} />
      <div className="tab-container">
        {TABS.map((item) => (
          <div
            key={item.title}
            className={`tab-item ${
              item.title === activeTab.title ? 'active' : ''
            }`}
            onClick={() => setActiveTab(item)}
          >
            {item.title}
          </div>
        ))}
      </div>
      <div className="frag-container">
        <AboutFrag selectedApp={selectedApp} colorCode={colorCode} />
      </div>
    </div>
  );
};

export default ViralTV;

const TABS = [
  { title: 'About' },
  { title: 'Comments' },
  { title: 'Category Videos' },
  { title: 'Share' },
];
