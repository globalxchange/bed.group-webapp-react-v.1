import { useContext, useState } from 'react';
import { AppContext } from '../../contexts/AppContext';
import ViralTV from '../ViralTV';

const BrandSideBar = ({ selectedApp, colorCode }) => {
  const { appData } = useContext(AppContext);

  const [isTvOpen, setIsTvOpen] = useState(false);

  const onVideoClick = () => {
    setIsTvOpen(true);
  };

  if (isTvOpen) {
    return (
      <ViralTV
        colorCode={colorCode}
        selectedApp={selectedApp}
        onClose={() => setIsTvOpen(false)}
      />
    );
  }

  return (
    <div className="brand-side-bar-wrapper">
      <img
        src={selectedApp?.other_data?.coloredfulllogo}
        alt=""
        className="header-icon"
      />
      <div className="hello-text">
        Hello, I Am {selectedApp?.name}
        <br />
        <span>App Here Is An Introductory Video To Explain What I Do</span>
      </div>
      <div className="video-container">
        <div className="thumbnail" onClick={onVideoClick}>
          <svg
            className="play-icon"
            width="78"
            height="87"
            viewBox="0 0 78 87"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M75.79 39.7069L6.45667 0.556966C5.11333 -0.199933 3.47533 -0.182533 2.14933 0.591766C0.814667 1.37476 0 2.80156 0 4.35016V82.65C0 84.1986 0.814667 85.6254 2.14933 86.4084C2.82533 86.7999 3.57933 87 4.33333 87C5.06133 87 5.798 86.8173 6.45667 86.4432L75.79 47.2933C77.1507 46.519 78 45.0748 78 43.5001C78 41.9254 77.1507 40.4812 75.79 39.7069Z"
              fill={colorCode || '#08152D'}
            />
          </svg>
        </div>
        <div className="video-details">
          <div className="video-title">Introducing {selectedApp?.name}</div>
          <div className="video-author">
            By :{' '}
            <div className="author-name">
              <img src={appData?.icon} alt="" className="author-icon" />{' '}
              {appData?.groupname}
            </div>
          </div>
          <div className="video-desc">
            {selectedApp.description.substring(0, 100)}
          </div>
        </div>
      </div>
      <div className="action-list-title">
        Which Of The Following Engagements Best Describes What You Are Looking
        For?
      </div>
      <div className="action-list">
        {ACTIONS.map((item) => (
          <div key={item.title} className="action-item">
            <img src={item.icon} alt="" className="action-icon" />
            <div className="d-flex flex-column flex-fill">
              <div className="action-name">{item.title}</div>
              <div className="action-desc">{item.subTitle}</div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default BrandSideBar;

const ACTIONS = [
  {
    title: 'Im Looking For Employment',
    subTitle: 'We Are Always Looking For Talented People',
    icon: require('../../assets/brandActionIcons/looking-employement.svg')
      .default,
  },
  {
    title: 'Im Looking To Invest',
    subTitle: 'We Are Always Looking For Likeminded Partners',
    icon: require('../../assets/brandActionIcons/looking-invest.svg').default,
  },
  {
    title: 'Im A Potential Customer',
    subTitle: 'Great. We Love Customers!',
    icon: require('../../assets/brandActionIcons/iam-customer.svg').default,
  },
  {
    title: 'Im A Potential Affiliate',
    subTitle: 'We Love That You Love Our Products That Much',
    icon: require('../../assets/brandActionIcons/iam-affliate.svg').default,
  },
  {
    title: 'Im A Potential Partner',
    subTitle: 'Wow. How Did You Know That We Needed More Partners',
    icon: require('../../assets/brandActionIcons/iam-partener.svg').default,
  },
  {
    title: 'Im Just Curious',
    subTitle: 'We Are Just As Curious About You',
    icon: require('../../assets/brandActionIcons/iam-curious.svg').default,
  },
];
