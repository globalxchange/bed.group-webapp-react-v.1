/* eslint-disable react/jsx-props-no-spreading */
import React, { useContext, useEffect, useState } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { AppContext } from './contexts/AppContext';
import AppDetailsPage from './pages/AppDetailsPage';
import LandingPage from './pages/LandingPage';
import DocumentMeta from 'react-document-meta';
import BrandsPage from './pages/BrandsPage';

const App = () => {
  const { appData, brandList } = useContext(AppContext);

  const [meta, setMeta] = useState('');

  useEffect(() => {
    if (appData) {
      const favicon = document.getElementById('favicon');
      favicon.href = appData.icon;

      const metaFromAPI = {
        title: appData?.other_data?.websitetitle || '',
        description: appData?.other_data?.websitedescription || '',
      };
      setMeta(metaFromAPI);
    }
  }, [appData]);

  return (
    <DocumentMeta {...meta}>
      <BrowserRouter>
        <Switch>
          {brandList?.map((item, index) =>
            item.disableRoute ? null : (
              <Route
                key={index}
                path={`/brands/${item.brand_code}`}
                exact
                component={(props) => (
                  <AppDetailsPage {...props} appData={item} />
                )}
              />
            ),
          )}
          <Route path="/brands" component={BrandsPage} />
          <Route path="/" component={LandingPage} />
        </Switch>
      </BrowserRouter>
    </DocumentMeta>
  );
};

export default App;
